from flask import Flask, render_template, request

from processing import process

app = Flask(__name__)


@app.route("/", methods=["get", "post"])  # http://127.0.0.1:5000 + "/"
def index():
    message = ""
    if request.method == "POST":
        area = request.form.get("area")
        try:
            area = float(area)
            cost = process(area)
            message = f"Стоимость недвижимости площадью {area} кв. м. равна {cost} руб."
        except Exception as e:
            message = "Введено некорректное значение"
            print(e)

    return render_template("index.html", message=message)


app.run()
