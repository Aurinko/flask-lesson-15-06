from pickle import load

import numpy as np


def process(params):
    regressor, scaler_x, scaler_y = _load_models()

    params = scaler_x.transform([[params]])
    cost = regressor.predict(params)

    cost = scaler_y.inverse_transform([cost])[0][0]
    cost = np.round(cost, 2)

    return cost


def _load_models(model_path="./model/rf.pkl",
                 scaler_x_path="./model/scaler_x.pkl",
                 scaler_y_path="./model/scaler_y.pkl"):
    regressor = _load_model(model_path)
    scaler_x = _load_model(scaler_x_path)
    scaler_y = _load_model(scaler_y_path)

    return regressor, scaler_x, scaler_y


def _load_model(path):
    with open(path, "rb") as f:
        model = load(f)

    return model
